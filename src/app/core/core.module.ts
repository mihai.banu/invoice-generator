import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContainerComponent } from './container/container.component';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CreateInvoiceFormComponent } from '../features/invoice-operations/components/create-invoice-form/create-invoice-form.component';
import { CreateClientFormComponent } from '../features/invoice-operations/components/create-client-form/create-client-form.component';
import { EditInvoiceFormComponent } from '../features/invoice-operations/components/edit-invoice-form/edit-invoice-form.component';
import { DeleteInvoiceFormComponent } from '../features/invoice-operations/components/delete-invoice-form/delete-invoice-form.component';
import { ToastsComponent } from './toasts/toasts.component';

@NgModule({
  declarations: [
    ContainerComponent,
    HeaderComponent,
    ToastsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ModalModule.forRoot()
  ],
  entryComponents: [
    CreateInvoiceFormComponent,
    EditInvoiceFormComponent,
    DeleteInvoiceFormComponent,
    CreateClientFormComponent
  ],
  exports: [
    ContainerComponent,
    ToastsComponent
  ]
})
export class CoreModule { }
