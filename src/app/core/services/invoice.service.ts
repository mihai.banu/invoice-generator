import { Injectable } from '@angular/core';
import { of, BehaviorSubject } from 'rxjs';
import { Client } from 'src/app/root-store/invoice/invoice.model';
import { InvoiceStoreFacade } from 'src/app/root-store/invoice/invoice-store.facade';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {


  public notifier$ = new BehaviorSubject<any>({});

  constructor(private invoiceStoreFacade: InvoiceStoreFacade) {}

  getClients() {
    const clients: Client[] = localStorage.getItem('clients') ? JSON.parse(localStorage.getItem('clients')) : [];
    return of(clients);
  }

  createClient(data: Client) {
    const clients: Client[] = localStorage.getItem('clients') ? JSON.parse(localStorage.getItem('clients')) : [];
    clients.push(data);
    localStorage.setItem('clients', JSON.stringify(clients));
    this.notifier$.next(true);
    return of({ msg: 'CREATED' })
      .pipe(
        // Load updated clients to store
        tap(() => this.invoiceStoreFacade.loadClients()));
  }

  getInvoices() {
    const invoices: any[] = localStorage.getItem('invoices') ? JSON.parse(localStorage.getItem('invoices')) : [];
    return of(invoices);
  }

  createInvoice(data: any) {
    const invoices: Client[] = localStorage.getItem('invoices') ? JSON.parse(localStorage.getItem('invoices')) : [];
    invoices.push(data);
    localStorage.setItem('invoices', JSON.stringify(invoices));
    this.notifier$.next(true);
    return of({ msg: 'CREATED' });
  }

  updateInvoice(selectedInvoice, data) {
    const invoices: Client[] = localStorage.getItem('invoices') ? JSON.parse(localStorage.getItem('invoices')) : [];
    // Find Invoice Index
    const index = invoices.findIndex((elem) => elem.id === selectedInvoice.id);
    // Update Invoice with new data
    invoices[index] = {...data, id: selectedInvoice.id};
    localStorage.setItem('invoices', JSON.stringify(invoices));
    this.notifier$.next(true);
    return of({ msg: 'UPDATED' });
  }

  deleteInvoice(selectedInvoice) {
    const invoices: Client[] = localStorage.getItem('invoices') ? JSON.parse(localStorage.getItem('invoices')) : [];
    // Find Invoice Index
    const index = invoices.findIndex((elem) => elem.id === selectedInvoice.id);
    // Delete Invoice at position index
    invoices.splice(index, 1);
    localStorage.setItem('invoices', JSON.stringify(invoices));
    this.notifier$.next(true);
  }
}
