import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import { map, take } from 'rxjs/operators';
import { AuthStoreFacade } from 'src/app/root-store/auth/auth-store.facade';

// Logic to restrict access to route
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authFacade: AuthStoreFacade, private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    const redirectTo = route.data.redirectTo || '/login';
    const disallowAccess = route.data.disallowAccess || false;
    // return this.authFacade.token$.pipe(
    //       map((token) => {
    //         const tree = this.router.parseUrl(redirectTo);
    //         if ((disallowAccess && token) || (!disallowAccess && !token)) {
    //           return tree;
    //         }
    //         return true;
    //       }),
    //       take(1),
    //     );
    return true;
  }
}
