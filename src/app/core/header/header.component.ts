import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap';

import { CreateInvoiceFormComponent } from '@features/invoice-operations/components/create-invoice-form/create-invoice-form.component';
import { CreateClientFormComponent } from '@features/invoice-operations/components/create-client-form/create-client-form.component';
import { DeleteInvoiceFormComponent } from '@features/invoice-operations/components/delete-invoice-form/delete-invoice-form.component';
import { EditInvoiceFormComponent } from '@features/invoice-operations/components/edit-invoice-form/edit-invoice-form.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private modalService: BsModalService) { }

  ngOnInit() {
  }

  private createInvoice() {
    this.modalService.show(CreateInvoiceFormComponent, {class: 'modal-lg', backdrop : 'static'});
  }

  private editInvoice() {
    this.modalService.show(EditInvoiceFormComponent, {class: 'modal-lg', backdrop : 'static'});
  }

  private deleteInvoice() {
    this.modalService.show(DeleteInvoiceFormComponent, {class: 'modal-lg', backdrop : 'static'});
  }

  private createClient() {
    this.modalService.show(CreateClientFormComponent, {class: 'modal-lg', backdrop : 'static'});
  }
}
