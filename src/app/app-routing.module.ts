import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';
import { ExportPageComponent } from '@features/invoice-operations/pages/export-page/export-page.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'login', pathMatch: 'full'
  },
  // Lazy loading the invoice operations module
  {
    path: 'invoice',
    canActivate: [AuthGuard],
    loadChildren: () => import('./features/invoice-operations/invoice-operations.module').then(m => m.InvoiceOperationsModule)
  },
  // Lazy loading the login module
  {
    path: 'login',
    loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)
  },
  { path: 'export/:id', component: ExportPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
