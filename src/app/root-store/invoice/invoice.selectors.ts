import { createFeatureSelector, createSelector } from '@ngrx/store';

import { State, adapter, INVOICE_FEATURE_KEY } from './invoice.reducer';

// Lookup the 'Client' feature state managed by NgRx
const getClientState = createFeatureSelector<State>(INVOICE_FEATURE_KEY);

// get the selectors
const { selectIds, selectAll, selectTotal } = adapter.getSelectors();

// select the array of Client ids
export const selectClientIds = createSelector(
  getClientState,
  selectIds
);

// select the array of Clients
export const selectAllClients = createSelector(
  getClientState,
  selectAll
);

// select the total Client count
export const selectClientCount = createSelector(
  getClientState,
  selectTotal
);

// select Client loaded flag
export const selectClientLoaded = createSelector(
  getClientState,
  state => state.loaded
);

// select Client error
export const selectError = createSelector(
  getClientState,
  state => state.error
);
