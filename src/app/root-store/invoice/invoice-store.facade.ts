import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import { Observable } from 'rxjs';
import { selectAllClients } from './invoice.selectors';
import { fromClientActions } from './invoice.actions';
import { Client } from './invoice.model';

@Injectable({
  providedIn: 'root'
})
export class InvoiceStoreFacade {

  public get clients$(): Observable<Client[]> {
    return this.store.select(selectAllClients);
  }

  constructor(private store: Store<any>) {
  }

  public loadClients() {
    this.store.dispatch(fromClientActions.loadClients());
  }

}
