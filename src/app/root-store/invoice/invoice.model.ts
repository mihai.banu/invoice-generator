export class Client {
  id: number;
  name: string;
  address: {
    houseNumber: string;
    street: string;
    city: string;
    state: string;
    country: string;
  };
}
