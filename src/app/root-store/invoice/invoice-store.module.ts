import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { InvoiceEffects } from './invoice.effects';
import { invoiceReducer } from './invoice.reducer';

@NgModule({
  imports: [
    StoreModule.forFeature('invoice', invoiceReducer),
    EffectsModule.forFeature([InvoiceEffects])
  ]
})
export class InvoiceStoreModule {

}
