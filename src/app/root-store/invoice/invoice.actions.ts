import { createAction, props } from '@ngrx/store';
import { Client } from './invoice.model';

export enum ClientActionTypes {
  LoadClients = '[Client] Load Clients',
  LoadClientsSuccess = '[Client] Load Clients Success',
  LoadClientsFail = '[Client] Load Clients Fail'
}

export const loadClients = createAction(ClientActionTypes.LoadClients);

export const loadClientsSuccess = createAction(
  ClientActionTypes.LoadClientsSuccess,
  props<{ data: Client[] }>()
);

export const loadClientsFail = createAction(
  ClientActionTypes.LoadClientsFail,
  props<{ error: Error | any }>()
);

export const fromClientActions = {
  loadClients,
  loadClientsFail,
  loadClientsSuccess
};
