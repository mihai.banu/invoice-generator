import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { fromClientActions } from './invoice.actions';
import { Client } from './invoice.model';

export const INVOICE_FEATURE_KEY = 'invoice';

export interface State extends EntityState<Client> {
  loaded: boolean;
  error?: Error | any;
}

export const adapter: EntityAdapter<Client> = createEntityAdapter<Client>({
  // In this case this would be optional since primary key is id
  selectId: item => item.id
});

export interface ClientPartialState {
  readonly [INVOICE_FEATURE_KEY]: State;
}

export const initialState: State = adapter.getInitialState({
  // Additional client state properties
  loaded: false,
  error: null
});

const InvoiceReducer = createReducer(
  initialState,
  on(fromClientActions.loadClientsSuccess, (state, { data }) => {
    return adapter.addAll(data, {
      ...state,
      loaded: true
    });
  }),
  on(fromClientActions.loadClientsFail, (state, { error }) => {
    return {
      ...state,
      error
    };
  }),
  // on(fromClientActions.loadClientSuccess, (state, { id, item }) => {
  //   return adapter.addOne(item, state);
  // }),
  // on(fromClientActions.loadClientFail, (state, { error }) => {
  //   return {
  //     ...state,
  //     error
  //   };
  // })
);

export function invoiceReducer(state: State | undefined, action: Action) {
  return InvoiceReducer(state, action);
}
