import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { fromClientActions } from './invoice.actions';
import { InvoiceService } from '../../core/services/invoice.service';

@Injectable()
export class InvoiceEffects {
  loadClients$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fromClientActions.loadClients),
      switchMap(() =>
        this.invoiceService.getClients().pipe(
          map((res: any) =>
            fromClientActions.loadClientsSuccess({
              data: res
            })
          ),
          catchError(error =>
            of(
              fromClientActions.loadClientsFail({
                error
              })
            )
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private invoiceService: InvoiceService
  ) {}
}
