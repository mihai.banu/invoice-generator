import { Action, createReducer, on } from '@ngrx/store';
import * as AuthActions from './auth.actions';

export interface AuthState {
  error: any;
  loading: boolean;
  token: string;
}

export const authInitialState: AuthState = {
  error: null,
  loading: false,
  token: ''
};

const AuthReducer = createReducer(
  authInitialState,
  on(AuthActions.login, state => ({ ...state, loading: true })),
  on(AuthActions.loginSuccess, (state, updatedVal) => ({ ...state, loading: false, token: updatedVal.token })),
  on(AuthActions.loginFail, state => ({ ...state, loading: false, error: true })),
  on(AuthActions.register, state => ({ ...state, loading: true, error: null })),
  on(AuthActions.registerSuccess, (state, updatedVal) => ({ ...state, loading: false, error: null, token: updatedVal.token })),
  on(AuthActions.registerFail, state => ({ ...state, loading: false, error: true }))
);

export function authReducer(state: AuthState, action: Action) {
  return AuthReducer(state, action);
}
