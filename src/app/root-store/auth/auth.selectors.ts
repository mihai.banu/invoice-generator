import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState } from './auth.reducer';

export const authFeatureSelector = createFeatureSelector('auth');

export const selectToken = createSelector(
  authFeatureSelector,
  (state: AuthState) => state.token
);
