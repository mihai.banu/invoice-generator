import { createAction, props } from '@ngrx/store';

export const login = createAction(
  '[Auth] Login',
  props<{ username: string; password: string }>()
);

export const loginSuccess = createAction(
  '[Auth] Login Success',
  props<{ token: string; }>()
);

export const loginFail = createAction(
  '[Auth] Login Fail',
  props<{ error: string; }>()
);

export const register = createAction(
  '[Auth] Register',
  props<{ username: string; email: string; password: string; confirmPassword: string }>()
);

export const registerSuccess = createAction(
  '[Auth] Register Success',
  props<{ token: string; }>()
);

export const registerFail = createAction(
  '[Auth] Register Fail',
  props<{ error: string; }>()
);
