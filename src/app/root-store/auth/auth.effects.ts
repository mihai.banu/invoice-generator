import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, switchMap } from 'rxjs/operators';
import { login, loginSuccess, loginFail, register, registerSuccess, registerFail } from './auth.actions';

const MOCK_TOKEN = '4aaad767-7a80-496a-ba89-fbaca21b8821';
@Injectable()
export class AuthEffects {
  constructor(private actions$: Actions) {
  }

  @Effect()
  public loginEffect() {
    return this.actions$
      .pipe(
        ofType(login),
        switchMap((data) => {
          // Mocking backend - Check user info
          const users: any = localStorage.getItem('users') ? JSON.parse(localStorage.getItem('users')) : [];
          const currentUser = users ? users.find(elem => elem.username === data.username) : null;
          if (!currentUser) {
            return [loginFail({error: 'Incorrect credentials'})];
          } else if (currentUser.password !== data.password) {
            return [loginFail({error: 'Incorrect credentials'})];
          }
          return [loginSuccess({token: MOCK_TOKEN})];
        }),
        catchError((err) => [loginFail({error: err.error})])
      );
  }

  @Effect()
  public registerEffect() {
    return this.actions$
      .pipe(
        ofType(register),
        switchMap((data) => {
          // Mocking backend - Store user info if it not exists already
          const users: any = localStorage.getItem('users') ? JSON.parse(localStorage.getItem('users')) : [];
          if (users.find(elem => elem.username === data.username)) {
            return [registerFail({error: 'User already exists!'})];
          }
          users.push({ username: data.username, email: data.email, password: data.password });
          localStorage.setItem('users', JSON.stringify(users));
          return [registerSuccess({token: MOCK_TOKEN})];
        }),
        catchError((err) => [registerFail({error: err.error})])
      );
  }

}
