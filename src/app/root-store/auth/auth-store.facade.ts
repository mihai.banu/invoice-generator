import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {login, register} from './auth.actions';
import {selectToken} from './auth.selectors';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthStoreFacade {

  public get token$(): Observable<string> {
    return this.store.select(selectToken);
  }

  constructor(private store: Store<any>) {
  }

  public login(data) {
    this.store.dispatch(login(data));
  }

  public register(data) {
    this.store.dispatch(register(data));
  }

}
