import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AuthStoreFacade } from 'src/app/root-store/auth/auth-store.facade';
import { MustMatch } from 'src/app/shared/helpers/must-match.validator';
import { ofType, Actions } from '@ngrx/effects';
import { Router } from '@angular/router';
import { registerSuccess, registerFail } from 'src/app/root-store/auth/auth.actions';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})
export class RegistrationFormComponent implements OnInit {

  submitted = false;
  registrationForm: FormGroup;
  error;

  constructor( private formBuilder: FormBuilder, private modalRef: BsModalRef, private authFacade: AuthStoreFacade,
               private actions: Actions, private router: Router ) { }

  ngOnInit() {
    // Creating reactive form
    this.registrationForm = this.formBuilder.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });

    // Listening to the registration events
    this.actions.pipe(ofType(registerSuccess)).subscribe(() => {
      this.router.navigate(['/invoice/home']);
      this.modalRef.hide();
    });

    this.actions.pipe(ofType(registerFail)).subscribe((msg) => {
      this.error = msg.error;
    });
  }

  // Getter for easy access to form fields
  get f() { return this.registrationForm.controls; }

  submitForm() {
    this.submitted = true;

    // Stop here if form is invalid
    if (this.registrationForm.invalid) {
      return;
    }

    this.authFacade.register(this.registrationForm.value);
  }

  close() {
    this.modalRef.hide();
  }

  reset() {
    this.submitted = false;
    this.registrationForm.reset();
  }


}
