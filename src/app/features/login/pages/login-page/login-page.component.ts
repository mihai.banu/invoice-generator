import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalService } from 'ngx-bootstrap';

import { RegistrationFormComponent } from '../../components/registration-form/registration-form.component';
import { AuthStoreFacade } from 'src/app/root-store/auth/auth-store.facade';
import { Actions, ofType } from '@ngrx/effects';
import { loginSuccess, loginFail } from 'src/app/root-store/auth/auth.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  submitted = false;
  loginForm: FormGroup;
  incorrectCredentials;
  constructor(private formBuilder: FormBuilder, private authFacade: AuthStoreFacade, private modalService: BsModalService,
              private actions: Actions, private router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username:  ['', Validators.required],
      password:  ['', Validators.required]
    });

    this.actions.pipe(ofType(loginSuccess)).subscribe(() => {
      this.incorrectCredentials = false;
      this.router.navigate(['/invoice/home']);
    });

    this.actions.pipe(ofType(loginFail)).subscribe((msg) => {
      this.incorrectCredentials = msg.error;
      this.router.navigate(['/']);
    });
  }

  // Getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  submitForm() {
    this.submitted = true;

    // Stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.authFacade.login(this.loginForm.value);
  }

  register() {
    this.modalService.show(RegistrationFormComponent, { class: 'modal-lg' });
  }

}
