import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { InvoiceService } from '@core/services/invoice.service';
import { combineLatest } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import * as jsPDF from 'jsPDF';

@Component({
  selector: 'app-export-page',
  templateUrl: './export-page.component.html',
  styleUrls: ['./export-page.component.scss']
})
export class ExportPageComponent implements OnInit {

  invoice;
  client;
  total;
  today = new Date();
  nextWeek = new Date();

  @ViewChild('pdfTemplate', {static: false}) pdfTemplate: ElementRef;

  constructor(private invoiceService: InvoiceService, private  route: ActivatedRoute) { }

  ngOnInit() {
    this.nextWeek.setDate(this.nextWeek.getDate() + 7);
    combineLatest(this.invoiceService.getInvoices(), this.invoiceService.getClients())
      .subscribe(([invoices, clients]) => {
        // Set data for template
        const id = this.route.snapshot.paramMap.get('id');
        this.invoice = invoices.find((elem) => elem.id === id);
        this.client = clients.find((elem) => elem.id === this.invoice.client);
        this.total = this.invoice.items.reduce((acc,cur) => acc + parseInt(cur.amount) ? parseInt(cur.amount) : 0, 0);
      });
  }

  printPdf() {
    const doc = new jsPDF();

    const specialElementHandlers = {
      '#editor': (element, renderer) => {
        return true;
      }
    };

    const pdfTemplate = this.pdfTemplate.nativeElement;

    doc.fromHTML(pdfTemplate.innerHTML, 15, 15, {
      width: 190,
      elementHandlers: specialElementHandlers
    });

    doc.save('invoice-export.pdf');
  }

}
