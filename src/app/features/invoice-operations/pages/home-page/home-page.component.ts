import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { InvoiceStoreFacade } from 'src/app/root-store/invoice/invoice-store.facade';
import { InvoiceService } from '@core/services/invoice.service';
import { Observable, combineLatest } from 'rxjs';
import { Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  invoices;
  clients;

  constructor( private invoiceService: InvoiceService,
               private router: Router, private changeDetector: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.invoiceService.notifier$.
      pipe(
        switchMap(() => combineLatest(this.invoiceService.getClients() , this.invoiceService.getInvoices()))
      )
      .subscribe(([clients, invoices]) => {
        this.clients = clients;
        this.invoices = invoices;
        this.changeDetector.detectChanges();
      }
    );
  }

  findClient(id) {
    if (this.clients) {
      return this.clients.find((elem) => elem.id === id);
    }
  }

  takeToPrint(id) {
    this.router.navigate(['/export', id]);
  }

}
