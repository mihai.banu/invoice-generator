import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInvoiceFormComponent } from './edit-invoice-form.component';

describe('EditInvoiceFormComponent', () => {
  let component: EditInvoiceFormComponent;
  let fixture: ComponentFixture<EditInvoiceFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditInvoiceFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditInvoiceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
