import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { combineLatest } from 'rxjs';
import { InvoiceService } from '@core/services/invoice.service';
import { Client } from 'src/app/root-store/invoice/invoice.model';

@Component({
  selector: 'app-edit-invoice-form',
  templateUrl: './edit-invoice-form.component.html',
  styleUrls: ['./edit-invoice-form.component.scss']
})
export class EditInvoiceFormComponent implements OnInit {

  submitted;
  clients: Client[];
  invoices: any;

  private selectUndefinedOptionValue: any;
  invoiceItemsForm: FormGroup;
  items: FormArray;
  selected = false;

  constructor(private formBuilder: FormBuilder, private invoiceService: InvoiceService, private modalRef: BsModalRef) {}

  ngOnInit() {
    this.invoiceItemsForm = this.formBuilder.group({
      client: ['', Validators.required],
      items: this.formBuilder.array([ this.createItem() ])
    });
    combineLatest(this.invoiceService.getClients(), this.invoiceService.getInvoices())
      .subscribe(([clients, invoices]) => {
      this.clients = clients;
      this.invoices = invoices;
    });
  }

  // Getter for easy access to form fields
  get f() { return this.invoiceItemsForm.controls; }

  createItem(): FormGroup {
    return this.formBuilder.group({
      description: ['', Validators.required],
      cost: ['', Validators.required],
      quantity: ['', Validators.required],
      amount: ['', Validators.required]
    });
  }

  addItem(): void {
    this.items = this.invoiceItemsForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }

  selectInvoice($event): void {
    this.selected = $event;
    this.invoiceItemsForm.patchValue($event);
  }

  submitForm(): void {
    this.submitted = true;
    this.invoiceService.updateInvoice(this.selected, this.invoiceItemsForm.value);
    this.close();
  }

  close() {
    this.modalRef.hide();
  }

  reset() {
    this.submitted = false;
    this.invoiceItemsForm.reset();
  }

}
