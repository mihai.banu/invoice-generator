import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteInvoiceFormComponent } from './delete-invoice-form.component';

describe('DeleteInvoiceFormComponent', () => {
  let component: DeleteInvoiceFormComponent;
  let fixture: ComponentFixture<DeleteInvoiceFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteInvoiceFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteInvoiceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
