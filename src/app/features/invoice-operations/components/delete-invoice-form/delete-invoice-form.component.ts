import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { InvoiceService } from '@core/services/invoice.service';

@Component({
  selector: 'app-delete-invoice-form',
  templateUrl: './delete-invoice-form.component.html',
  styleUrls: ['./delete-invoice-form.component.scss']
})
export class DeleteInvoiceFormComponent implements OnInit {

  selected;
  invoices;

  private selectUndefinedOptionValue: any;
  constructor(private modalRef: BsModalRef, private invoiceService: InvoiceService) {}

  ngOnInit() {
    this.invoiceService.getInvoices().subscribe((invoices) => {
      this.invoices = invoices;
    });
  }

  selectInvoice($event): void {
    this.selected = $event;
  }

  deleteInvoice(): void {
    this.invoiceService.deleteInvoice(this.selected);
    this.close();
  }

  close() {
    this.modalRef.hide();
  }

}
