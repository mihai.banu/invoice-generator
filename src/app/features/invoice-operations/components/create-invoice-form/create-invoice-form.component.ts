import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { InvoiceStoreFacade } from 'src/app/root-store/invoice/invoice-store.facade';
import { InvoiceService } from '@core/services/invoice.service';
import { Client } from 'src/app/root-store/invoice/invoice.model';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-create-invoice-form',
  templateUrl: './create-invoice-form.component.html',
  styleUrls: ['./create-invoice-form.component.scss']
})
export class CreateInvoiceFormComponent implements OnInit {

  submitted;
  invoiceItemsForm: FormGroup;
  items: FormArray;
  clients: Client[];
  templates: any;
  selectedTemplate: any;

  constructor(private formBuilder: FormBuilder, private modalRef: BsModalRef,
              private invoiceStore: InvoiceStoreFacade, private invoiceService: InvoiceService) {}

  ngOnInit() {
    this.invoiceItemsForm = this.formBuilder.group({
      client: ['', Validators.required],
      items: this.formBuilder.array([ this.createItem() ])
    });

    combineLatest(this.invoiceService.getClients(), this.invoiceService.getInvoices())
      .subscribe(([clients, templates]) => {
      this.clients = clients;
      this.templates = templates;
    });
  }

  // Getter for easy access to form fields
  get f() { return this.invoiceItemsForm.controls; }

  createItem(): FormGroup {
    return this.formBuilder.group({
      description: ['', Validators.required],
      cost: ['', Validators.required],
      quantity: ['', Validators.required],
      amount: ['', Validators.required]
    });


  }

  addItem(): void {
    this.items = this.invoiceItemsForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }

  submitForm(): void {
    this.submitted = true;
    this.invoiceService.createInvoice({id: this.generateId(), ...this.invoiceItemsForm.value});
    this.modalRef.hide();
  }

  close() {
    this.modalRef.hide();
  }

  reset() {
    this.submitted = false;
    this.invoiceItemsForm.reset();
  }

  useTemplate() {
    this.invoiceItemsForm.patchValue(this.selectedTemplate);
  }

  generateId() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

}
