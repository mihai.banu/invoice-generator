import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateInvoiceFormComponent } from './create-invoice-form.component';

describe('CreateInvoiceFormComponent', () => {
  let component: CreateInvoiceFormComponent;
  let fixture: ComponentFixture<CreateInvoiceFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateInvoiceFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateInvoiceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
