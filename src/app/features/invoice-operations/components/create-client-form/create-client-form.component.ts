import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { InvoiceService } from '@core/services/invoice.service';

@Component({
  selector: 'app-create-client-form',
  templateUrl: './create-client-form.component.html',
  styleUrls: ['./create-client-form.component.scss']
})
export class CreateClientFormComponent implements OnInit {

  submitted;

  clientForm: FormGroup;
  items: FormArray;

  constructor(private formBuilder: FormBuilder, private modalRef: BsModalRef, private invoiceService: InvoiceService) {}

  ngOnInit() {
    this.clientForm = this.formBuilder.group({
      name: ['', Validators.required],
      address: this.formBuilder.group({
        houseNumber: '',
        street: '',
        city: ['', Validators.required],
        state: '',
        country: ['', Validators.required]
      })
    });
  }

  // Getter for easy access to form fields
  get f() { return this.clientForm.controls; }
  get g() { return this.clientForm.controls.address['controls']; }

  submitForm(): void {
    this.submitted = true;
    this.invoiceService.createClient({id: this.generateId(), ...this.clientForm.value});
    this.close();
  }

  close() {
    this.modalRef.hide();
  }

  reset() {
    this.submitted = false;
    this.clientForm.reset();
  }

  generateId() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

}
