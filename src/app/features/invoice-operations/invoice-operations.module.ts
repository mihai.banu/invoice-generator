import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InvoiceOperationsRoutingModule } from './invoice-operations-routing.module';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CoreModule } from 'src/app/core/core.module';
import { CreateInvoiceFormComponent } from './components/create-invoice-form/create-invoice-form.component';
import { CreateClientFormComponent } from './components/create-client-form/create-client-form.component';
import { DeleteInvoiceFormComponent } from './components/delete-invoice-form/delete-invoice-form.component';
import { EditInvoiceFormComponent } from './components/edit-invoice-form/edit-invoice-form.component';
import { ExportPageComponent } from './pages/export-page/export-page.component';

@NgModule({
  declarations: [
    HomePageComponent,
    CreateInvoiceFormComponent,
    EditInvoiceFormComponent,
    DeleteInvoiceFormComponent,
    CreateClientFormComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    InvoiceOperationsRoutingModule
  ]
})
export class InvoiceOperationsModule { }
