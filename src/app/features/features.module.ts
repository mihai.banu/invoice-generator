import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginModule } from './login/login.module';
import { InvoiceOperationsModule } from './invoice-operations/invoice-operations.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    LoginModule,
    InvoiceOperationsModule,
  ]
})
export class FeaturesModule { }
