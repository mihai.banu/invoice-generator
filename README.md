# Invoice Generator

 Invoice Generator allows to quickly make invoices and download them as PDF.

 To install please clone and run npm install or yarn install.

 To start the project please run npm start or yarn start. 
 
 The app should be available on port 4200.

Features 
- Register to use the app
- Login
- Create client
- Create invoice based on existing template (previously created invoice)
- Create invoice for a created client
- Update invoice
- Delete invoice

Design Patterns / Libraries Used
- NgRx for State Management
- Facades
- Guards
- Observer / Subscriber
- Interceptors
- Bootstrap 4+ for Styling
- JsPdf for pdf download (Limitation here that this library does not support CSS styling so the exported pdf will look a bit basic)

To do
- Implement unsubscribers to avoid memory leaks
- Implement the remaining actions for the store
- Deploy to cloud

Backend is mocked. All data gets stored in the local storage.
